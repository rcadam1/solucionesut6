/* 6. Realiza el siguiente programa que permita generar un número aleatorio entre un mínimo y un máximo introducidos por el usuario en sendos recuadros de texto. El número generado se mostrará mediante una etiqueta al hacer clic sobre el botón. Observa la captura de la aplicación en "ejercicio6.jpg".

Incluye en la ventana dos paneles que contengan:
- el primero las etiquetas Mínimo y Máximo, así como ambos recuadros de texto
- el segundo incluirá el botón y la etiqueta con el resultado.

La disposición a utilizar en el primer panel será FlowLayout y en el segundo GridLayout de una columna (recuerda que para ello puedes utilizar el método setLayout de la contenedora de componentes)..*/

 import java.awt.*;
 import java.awt.event.*;
 import javax.swing.*;
 import javax.swing.event.*;
 class ventana
 {
 	private JFrame jf;
 	private JPanel jp1,jp2;

 	private JLabel jl1,jl2,jl3;
 	private JButton jb;
    private JTextField jt1,jt2;

 	public ventana()
 	{
 		jf = new JFrame("Generador de número aleatorio");
        jf.setLayout(new GridLayout(0,1));
 	  	jp1 = new JPanel(new FlowLayout());
 	  	jp2 = new JPanel(new GridLayout(0,1));

 		jl1 = new JLabel("Mínimo");
 		jl2 = new JLabel("Máximo");
 		jl3 = new JLabel("Número: ");
 		//jt1 = new JTextField("Indica mínimo");
        jt1 = new JTextField(5);
        //jt1.setSize(200,10);
        jt1.setMinimumSize(new Dimension(200,10));
        System.out.println(jt1.getSize());
 		jt2 = new JTextField("Indica máximo");
        jb = new JButton("Generar aleatorio");

		jf.add(jp1);
 		jf.add(jp2);		

     	jp1.add(jl1);
 		jp1.add(jt1);
 		jp1.add(jl2);
 		jp1.add(jt2);

 		jp2.add(jb);
 		jp2.add(jl3);

 		jf.setVisible(true);
 		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
 		jf.pack();
 		jf.setLocationRelativeTo(null);

        jb.addActionListener(new ActionListener()
        {  
            public void actionPerformed(ActionEvent e)
 			{
                generaNumero();
 			}
 		});
    }

    public void generaNumero()
    {
        double n1 = Double.parseDouble(jt1.getText());
        double n2 = Double.parseDouble(jt2.getText());

        jl3.setText("Número: " + ( (int) (n1 + (n2-n1+1) * Math.random()) )  );
    }
 
 }

 public class ej6
 {
 	public static void main(String[] args) {
 		ventana v = new ventana();
 	}
 }
