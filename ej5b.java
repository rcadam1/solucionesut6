/* 5a. Realiza una aplicación gráfica que permita a
 un bibliotecario listar las películas guardadas en el
  fichero ("films.dat") generado por el ejercicio 11 de
   la unidad anterior de entrada/salida. Para mostrar los
    datos de cada película has de utilizar componentes
     JTextField, de forma que con cada clic sobre un botón 
     "Siguiente" vaya mostrando en ellos los valores para cada 
     película. */
/* 5b. Repite el ejercicio leyendo los datos de las películas desde un fichero de texto (films.txt) que incluya cada película en una línea y separe los campos de cada película por una tabulación. */

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

class ventana extends JFrame implements ActionListener
{
	FileReader fr;
	BufferedReader br;
	JPanel jp1,jp2;
	JLabel jl1,jl2,jl3;
	JTextField jt1,jt2,jt3;
	JButton jb;

	public void iniciaDis()
	{
		try
		{
			fr = new FileReader("films.txt");
			br = new BufferedReader(fr);
		}
		catch(IOException ex)
		{
			System.err.println(ex.getMessage());
		}
	}
	public void colocaComponentes(LayoutManager lm)
	{
		setTitle("LISTA PELICULAS");
		setLayout(lm);
		jp1 = new JPanel(new GridLayout(3,3));
		jp2 = new JPanel(new FlowLayout());
		add(jp1);
		add(jp2);
		jl1 = new JLabel("Código:");
		jl2 = new JLabel("Título:");
		jl3 = new JLabel("Director:");
		jt1 = new JTextField();
		jt2 = new JTextField();
		jt3 = new JTextField();
		jb = new JButton("Siguiente");
		jp1.add(jl1); jp1.add(jt1);
		jp1.add(jl2); jp1.add(jt2);
		jp1.add(jl3); jp1.add(jt3);
		jp2.add(jb);
		setVisible(true);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		pack();
		jb.addActionListener(this);
		iniciaDis();
	}

	public void actionPerformed(ActionEvent e)
	{
        String linea;
		int cod=0; String tit="",dir="";
		try
		{
			linea = br.readLine();
            if (linea != null)
            {
                StringTokenizer st = new StringTokenizer(linea,"\t");
			    cod = Integer.parseInt(st.nextToken());tit = st.nextToken();dir = st.nextToken();
            }
		}
		catch(IOException ex)
		{
			System.err.println(ex.getMessage());
		}
		jt1.setText(String.valueOf(cod));
		jt2.setText(tit);
		jt3.setText(dir);
	}

/*	public void	windowClosed(WindowEvent e)
	{

	}*/
}

public class ej5b
{
	public static void main(String[] args) {
		ventana v = new ventana();
		v.colocaComponentes(new GridLayout(0,1));
	}
}
