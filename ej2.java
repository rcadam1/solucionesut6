/* 2. Realiza una aplicación que genere un número aleatorio
 al hacer clic sobre un botón. El número de dígitos que tendrá
  el aleatorio lo tomará de un recuadro de texto (JTextField).
Si el recuadro tiene el valor 3, por ejemplo, generará aleatorios
entre 0 y 999. Muestra el valor generado en una etiqueta.
Opcionalmente puedes hacer que se guarde en fichero cada uno
 de los valores generados.*/

 import java.awt.*;
 import java.awt.event.*;
 import javax.swing.*;
 import java.io.*;

 class ventana
 {
 	private JFrame jf;
 	private JPanel jp;
 	private JLabel jl1,jl2;
 	private JButton jb;
 	private JTextField jtf;

 	public ventana()
 	{
 		// creo los componentes
 		jf = new JFrame("Generador de sorteos");
 		jp = new JPanel(new GridLayout(2,2));
 		jl1 = new JLabel("Indica dígitos:");
 		jl2 = new JLabel("");
 		jtf = new JTextField("1");
 		jb = new JButton("Realiza un nuevo sorteo");
 		// añado componentes
 		jf.add(jp);	// añado el panel a la ventana
 		// añado el resto de componentes al panel
 		jp.add(jl1);
 		jp.add(jb);
 		jp.add(jtf);
 		jp.add(jl2);
 		jf.setVisible(true);
 		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
 		jf.pack();
 		jf.setLocationRelativeTo(null);

 		jb.addActionListener(new ActionListener()
 		{	// clase anónima
 			public void actionPerformed(ActionEvent e)
 			{
 				eventoBoton();
 			}
 		});
 	}

 	public void eventoBoton()
 	{
 		int digitos = Integer.parseInt(jtf.getText());
 		long limite = (long)Math.pow(10,digitos);	// 10 elevado a dígitos
 		System.out.println(limite);
 		long num = (long)(limite*Math.random());
 		jl2.setText("El número premiado es " + num);
 		guardaAFichero(num);
 	}

 	public void guardaAFichero(long num)
 	{
 		try(FileWriter fw = new FileWriter("historicoSorteos.txt",true))
 		{
 			fw.write(String.valueOf(num)+"\n");
 		}
 		catch (IOException e)
 		{
 			System.err.println(e.getMessage());
 		}
 	}
 }

 public class ej2
 {
 	public static void main(String[] args) {
 		ventana v = new ventana();
 	}
 }