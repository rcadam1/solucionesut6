/* 4. Programa que, utilizando 3 componentes JSlider con valores de 0 a 255, permita seleccionar graduación de color para los 3 colores RGB ( rojo, verde y azul ). El programa mostrará en otro componente (etiqueta o recuadro de texto, por ejemplo) el color resultante para los 3 valores escogidos de graduación. Puedes añadir también 3 componentes checkBox que permitan seleccionar o deseleccionar cada color por separado. Es decir, si un color no tiene su checkBox correspondiente seleccionado actuará como si el slider estuviera a cero.*/

 import java.awt.*;
 import java.awt.event.*;
 import javax.swing.*;
 import javax.swing.event.*;
 
 class ventana
 {
 	private JFrame jf;
 	private JPanel jp;
 	private JLabel jl1,jl2;

	static final int MIN = 0;
	static final int MAX = 255;
	static final int INIT = 0;
    private JSlider js1,js2,js3; 

 	public ventana()
 	{
 		jf = new JFrame("RGB");
 	  	jp = new JPanel(new GridLayout(0,1));
        js1 = new JSlider(JSlider.HORIZONTAL,MIN,MAX,INIT);
        js2 = new JSlider(JSlider.HORIZONTAL,MIN,MAX,INIT);
        js3 = new JSlider(JSlider.HORIZONTAL,MIN,MAX,INIT);
 		jl1 = new JLabel("RGB (0,0,0)");
 		jl2 = new JLabel();
        jl2.setOpaque(true);
        jl2.setBackground(new Color(0,0,0));
        //jl2.setForeground(new Color(255,0,0));
    
 		jf.add(jp);

 		jp.add(js1);
		js1.setBorder(BorderFactory.createTitledBorder("R"));
		js1.setMajorTickSpacing(85);
		js1.setMinorTickSpacing(5);
		js1.setPaintTicks(true);
		js1.setPaintLabels(true);

 		jp.add(js2);
		js2.setBorder(BorderFactory.createTitledBorder("G"));
		js2.setMajorTickSpacing(85);
		js2.setMinorTickSpacing(5);
		js2.setPaintTicks(true);
		js2.setPaintLabels(true);

 		jp.add(js3);
		js3.setBorder(BorderFactory.createTitledBorder("B"));
		js3.setMajorTickSpacing(85);
		js3.setMinorTickSpacing(5);
		js3.setPaintTicks(true);
		js3.setPaintLabels(true);

 		jp.add(jl1);
 		jp.add(jl2);

 		jf.setVisible(true);
 		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
 		jf.pack();
 		jf.setLocationRelativeTo(null);

 		js1.addChangeListener(new ChangeListener()
 		{	
 			public void stateChanged(ChangeEvent e)
 			{
                cambiaColor();
 			}
 		});

 		js2.addChangeListener(new ChangeListener()
 		{	
 			public void stateChanged(ChangeEvent e)
 			{
                cambiaColor();
 			}
 		});

 		js3.addChangeListener(new ChangeListener()
 		{	
 			public void stateChanged(ChangeEvent e)
 			{
                cambiaColor();
 			}
 		});
 	}

    public void cambiaColor()
    {
    	int v1=js1.getValue(), v2=js2.getValue(), v3=js3.getValue();
		jl1.setText("RGB (" + v1 + "," + v2  + "," + v3 + ")");
        jl2.setBackground(new Color(v1,v2,v3));
    }
 
 }

 public class ej4
 {
 	public static void main(String[] args) {
 		ventana v = new ventana();
 	}
 }
