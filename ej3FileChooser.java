import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;

public class ej3FileChooser
{
	private static JFrame ventana = null;
	private static JFileChooser jfc = null;
	private static JTextField jtf = null;
	private static JButton jb1 = null;
	private static JButton jb2 = null;
	private static File f = null;
	private static JPanel panel = null;

	public static void disponeComponentes()
	{
		ventana = new JFrame("Ejemplo FileChooser");
		panel = new JPanel();
		ventana.getContentPane().add(panel);
	    panel.setLayout(new GridLayout(0,1));
	    jtf = new JTextField();
	    jb1 = new JButton("Escoge fichero");
		jb2 = new JButton("Escribe en ese fichero");
	    panel.add(jtf);panel.add(jb1);panel.add(jb2);
	    jfc = new JFileChooser();
		ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ventana.pack();
		ventana.setLocationRelativeTo(null);
		ventana.setVisible(true);
	}

	public static void respondeEventos()
	{
		jb1.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				obtieneFichero();	
			}
		});
		jb2.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				try
				{
					FileWriter fw = new FileWriter(f,true);
					fw.write(jtf.getText());
					fw.close();
				}
				catch (IOException ex)
				{}
			}
		});

	}
	public static void obtieneFichero()
	{
		int returnVal = jfc.showOpenDialog(panel);

		if (returnVal == JFileChooser.APPROVE_OPTION)
            				f = jfc.getSelectedFile();
	}
	public static void main(String args[])
	{
		disponeComponentes();
	    respondeEventos();
	}
}
// fin
